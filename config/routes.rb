Rails.application.routes.draw do
  root to: 'pages#home'
  %w(
    efikasnost
    kontakt
    proizvodi
    galerija
    usluge
  ).each do |page|
    get page, to: "pages##{page}", as: page
  end
end
