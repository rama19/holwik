# HOLWIK

[![Build Status](https://semaphoreci.com/api/v1/projects/4ef55e4b-e014-4041-b69e-3ad9e98be111/617097/badge.svg)](https://semaphoreci.com/vlado/holwik)

## Getting started

Make sure you have:

1. Proper Ruby installed (check '.ruby-version')
2. PostgreSQL database installed
3. Bundler gem installed ('gem bundle install')
4. Foreman gem installed ('gem install foreman ')

Then do the following:

~~~~
git clone git@bitbucket.org:rama19/holwik.git
cd holwik
bundle install
rake db:create
rake db:schema:load
foreman start
open http://localhost:3000
~~~~

## Repository

Code is on [BitBucket](https://bitbucket.org/rama19/holwik/src)

## Deploy

To deploy to heroku use `git push heroku master`
